# Final Fantasy XIV

> Have you heard of the critically acclaimed MMORPG Final Fantasy XIV? With an expanded free trial which you can play through the entirety of A Realm Reborn and the award winning Heavensward expansion up to level 60 for free with no restrictions on playtime.

Parmi mes jeux préférés, il y a ce petit jeu peu connu qu'est Final Fantasy XIV. Contrairement à la plupart des jeux de cette licence, FF14 est un jeux en ligne qui existe depuis 2013 et qui est aujourd'hui un des plus gros MMORPG existants

## Mais du coup, c'est quoi ?

FFXIV, c'est surtout connu pour son histoire reconnue qui se déroule à travers ses différentes extensions (A Realm Reborn, Heavensward, Stormblood, Shadowbringers et Endwalker), des méchants qui font perdre tout compas moral et des moments qui ont juste envie de t'arracher toutes les émotions et les larmes de ton corps.

![EMET-SIMP](medias/images/EmetKholusia.jpg) 
*Quel beau gosse n'empêche...*

On sent que l'univers du jeu est vraiment vivant, aussi bien à travers ses personnages attachants (ou un peu moins *-tousse-* Papalymo et Yda *-tousse-*... ) avec qui on va tisser des liens en tant que Guerrier de la Lumière, son worldbuilding, les peuples et tribus et leur histoire propre, les différents événements ponctuels ou saisonniers qui animent le monde qu'à travers la communauté qui fait vivre le jeu aussi !

Il y a une grosse diversité d'activités possibles à faire une fois qu'on a avancé ou fini la dernière quête principale en date. Certains s'y retrouvent dans la mode ou la décoration d'intérieur, d'autres dans la récolte et l'artisanat (parce que oui, il y a de la pêche dans ce jeu) mais une importante partie de la communauté se retrouve régulièrement dans du contenu de haut-niveau pour affronter à plusieurs les instances les plus difficiles que propose le jeu avec chaque mise à jour majeure qui apporte de nouveaux donjons et raids

![jobs](medias/images/FFXIVJobs.jpg)
*De gauche à droite et de haut en bas, 8 classes de base du jeu: Maraudeur, Gladiateur, Lancier, Pugiliste, Archer, Élémentaliste, Occultiste et Arcaniste*

Sinon FFXIV c'est aussi un énorme fandom avec énormément de lore et son lot de RP un peu partout. Attention quand même à Balmung et à la place principale de Limsa Lominsa, on sait pas trop ce qui s'y passe... Chaque joueur a probablement sa propre histoire pour son Guerrier de la Lumière et même jobs, qu'il considère plus ou moins canon par rapport au lore de la trame principale ou ses propres ships. 

## Ma progression

De mon côté, j'ai fait mon petit bout de chemin dans toute cette aventure depuis Été 2020, malgré une pause entre l'Été 2021 et Printemps 2022

![postARR](medias/images/FFXIVNutshell.png)
