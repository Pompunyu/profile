# Hello World ![plante](medias/seedling.svg)

Bienvenue sur **Pompunyu Docs**. On peut dire que c'est un peu mon site fourre-tout

Je parle ici de mes hobbies et d'autres choses qui me plaisent mais c'est aussi là que tu trouveras mes infos pour me contacter !


La création de ce site n'avait pas vraiment d'ambition initiale ; en toute honnêteté, je voulais principalement tester un nouvel outil de dev pour voir le rendu et je pensais ne plus y toucher ensuite. Avec un peu de recul, je me suis dit que ça serait quand même bien pratique d'avoir un site perso, un minimum tenu à jour qui me servirait de référence et où je pourrais recenser tout ce qui me passe par la tête. C'est comme ça que *Pompunyu Docs* est né ! Je n'affirme pas que ce site soit magnifique, ni qu'il soit révolutionnaire, mais j'espère que tu y trouveras toutes les infos que tu cherches !

---

## Au fait, qui je suis ?

C'est vrai, ça - j'ai oublié les présentations ! Je m'appelle Ellie, jeune dev fullstack à plein temps de 22 ans qui est au passage addict au thé ! 
<br>
Tu verras souvent mon image accompagnée d'un pingouin couleur lavande ou d'un tanuki, une petite créature mythologique japonaise ressemblant à un raton-laveur, et c'est normal: ce sont mes deux mascottes ! Ils s'appellent respectivement Punyu et Pompoko. C'est de la fusion de ces deux noms que vient *Pompunyu*, mon pseudo sur internet -

Je suis une personne *Non-Binaire* et *Genderfluid*, ça veut dire que mes pronoms peuvent varier. Généralement, je précise aux gens ou utilise un avatar bien précis en ligne pour les indiquer mais n'hésite pas à demander si tu as un doute ou que j'ai oublié ! Si ce que je viens de dire n'a pas vraiment de sens pour toi, je te conseille de te référer à [ce lexique](https://wikitrans.co/lexique/) ou à [cet article](https://wikitrans.co/2020/01/07/la-non-binarite-cest-quoi/). Tout ça concerne évidemment ma vie perso mais quand même c'est important de prendre en compte tout ça et de rester informé, surtout si c'est pour s'adresser à moi après ![coeur](medias/blue-heart.svg)  